using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Danger : MonoBehaviour
{
    [SerializeField] private int damage = 1;
    [SerializeField] private float delay = 1f;
    private bool alreadyHit;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        health health = collision.GetComponent<health>();
        if (alreadyHit || health == null) return;
        health.TakeDamage(damage);
        alreadyHit = true;
        StartCoroutine(Wait(1f));
    }

    private IEnumerator Wait(float amount)
    {
        yield return new WaitForSeconds(amount);
        alreadyHit = false;
    }
}
