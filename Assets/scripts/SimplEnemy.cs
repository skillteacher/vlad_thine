using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplEnemy : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    [SerializeField] private bool isTornedRight;
    [SerializeField] private string GroundLayerName = "Ground";
    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        Vector2 velocity = new Vector2(isTornedRight ? speed : -speed , rb.velocity.y);
        rb.velocity = velocity;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer(GroundLayerName)) return;

        Flip();
    }

    private void Flip()
    {
        isTornedRight = !isTornedRight;
        transform.Rotate(0f, 180f, 0f);
    }
}
