using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class crown : MonoBehaviour
{
    [SerializeField] private int crownCost = 1;
    private game game;
    private bool isCollected = false;

    private void Start()
    {
        game = FindObjectOfType<game>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isCollected || game == null) return;
        isCollected = true;
        game.AddCrowns(crownCost);
        Destroy(gameObject);
    }
}
