using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class game : MonoBehaviour
{
    [SerializeField] private GameObject restartMenu;
    [SerializeField] private TMP_Text finalStore;
    [SerializeField] private string firstlevelName;
    private int crownCount = 0;
    private int currentMaxLives;

    private void Awake()
    {
        game[] anotherGameScript = FindObjectsOfType<game>();
        if(anotherGameScript.Length>1)
        {
            Destroy(gameObject);
        }    
        else
        {
            DontDestroyOnLoad(gameObject);
        }

        restartMenu.SetActive(false);
    }

    public void AddCrowns(int amount)
    {
        crownCount += amount;
        playerui.ui.SetCrown(crownCount);
    }

    public void GameOver(int maxLives)
    {
        Time.timeScale = 0f;
        restartMenu.SetActive(true);
        finalStore.text = crownCount.ToString();
        currentMaxLives = maxLives;
    }

    public void RestartGame()
    {
        crownCount = 0;
        playerui.ui.SetLives(currentMaxLives);
        SceneManager.LoadScene(firstlevelName);
        Time.timeScale = 1f;
        restartMenu.SetActive(false);
    }
}
