using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class health : MonoBehaviour
{
    [SerializeField] private int maxLives = 3;
    private Vector3 startPosition;
    private int lives;

    private void Start()
    {
        lives = maxLives;
        playerui.ui.SetLives(lives);
        startPosition = transform.position;
    }

    public void TakeDamage(int damage)
    {
        lives -= damage;
        playerui.ui.SetLives(lives);
        
        if(lives <= 0)
        {
            Death();
        }
        else
        {
            transform.position = startPosition;
        }
    }

    private void Death()
    {
        game game = FindObjectOfType<game>();
        game.GameOver(maxLives);
    }
}
