using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class playerui : MonoBehaviour
{
    public static playerui ui;

    [SerializeField] private TMP_Text livesCount;
    [SerializeField] private TMP_Text crownCount;

    private void Awake()
    {
        if(ui == null)
        {
            ui = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void SetLives(int amount)
    {
        if (amount < 0) amount = 0;
        livesCount.text = amount.ToString();
    }

    public void SetCrown(int amount)
    {
        crownCount.text = amount.ToString();
    }
}
